Rails.application.routes.draw do
  resources :items do
    put '/move', to: 'items#move', as: :move, on: :member
    put '/move-all', to: 'items#move_all', as: :move_all, on: :member
  end
  get '/warehouse/:name', to: 'items#warehouse', as: :warehouse
  delete '/warehouse/:name', to: 'items#close_warehouse'
  root to: 'items#index'
end
