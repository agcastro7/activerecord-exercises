class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    redirect_to items_path, flash: {success: 'Item was successfully created.'}
  end

  def update
    redirect_to items_path, flash: {success: 'Item was successfully updated.'}
  end

  def destroy
    @item.destroy
    redirect_to items_path, flash: {success: 'Item was successfully destroyed.'}
  end

  def warehouse
    @name = params[:name]
  end

  def move
    redirect_to items_path, flash: { success: 'Item moved successfully' }
  end

  def move_all
    redirect_to items_path, flash: { success: 'Items moved successfully' }
  end

  def close_warehouse
    @name = params[:name]
    redirect_to items_path, flash: { success: "Warehouse #{@name} closed" }
  end

  private
    def set_item
      @item = Item.new
    end

    def item_params
      params.require(:item).permit(:name, :description, :warehouse, :stock)
    end
end
