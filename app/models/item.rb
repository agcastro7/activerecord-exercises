class Item < ApplicationRecord


  def collapse
    self.reload
    @items = Item.where(name: name, warehouse: warehouse)

    # Aquí unificamos el stock de todos los items

    @items.where.not(id: self.id).delete_all
  end
end
